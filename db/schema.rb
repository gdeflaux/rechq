# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141223223938) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "api_cc_buffers", force: true do |t|
    t.string   "case_id"
    t.string   "case_type"
    t.string   "form_name"
    t.text     "data"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "facilities", force: true do |t|
    t.string   "name"
    t.string   "ancestry"
    t.float    "latitude"
    t.float    "longitude"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "population_total"
    t.integer  "population_under_five"
  end

  create_table "facilities_users", id: false, force: true do |t|
    t.integer "facility_id", null: false
    t.integer "user_id",     null: false
  end

  add_index "facilities_users", ["facility_id"], name: "index_facilities_users_on_facility_id", using: :btree
  add_index "facilities_users", ["user_id"], name: "index_facilities_users_on_user_id", using: :btree

  create_table "facility_followups", force: true do |t|
    t.integer  "facility_id"
    t.date     "follow_up_on"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id"
  end

  add_index "facility_followups", ["facility_id"], name: "index_facility_followups_on_facility_id", using: :btree
  add_index "facility_followups", ["user_id"], name: "index_facility_followups_on_user_id", using: :btree

  create_table "hardware_followups", force: true do |t|
    t.integer  "hardware_id"
    t.integer  "facility_id"
    t.integer  "software_id"
    t.text     "description"
    t.date     "followup_on"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "hardware_status_id"
    t.integer  "user_id"
  end

  add_index "hardware_followups", ["facility_id"], name: "index_hardware_followups_on_facility_id", using: :btree
  add_index "hardware_followups", ["hardware_id"], name: "index_hardware_followups_on_hardware_id", using: :btree
  add_index "hardware_followups", ["hardware_status_id"], name: "index_hardware_followups_on_hardware_status_id", using: :btree
  add_index "hardware_followups", ["software_id"], name: "index_hardware_followups_on_software_id", using: :btree
  add_index "hardware_followups", ["user_id"], name: "index_hardware_followups_on_user_id", using: :btree

  create_table "hardware_statuses", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "hardwares", force: true do |t|
    t.string   "code"
    t.date     "bought_on"
    t.date     "inventory_on"
    t.integer  "hardware_status_id"
    t.string   "brand"
    t.string   "model"
    t.string   "color"
    t.string   "configuration"
    t.string   "kind"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "facility_id"
    t.integer  "software_id"
  end

  add_index "hardwares", ["facility_id"], name: "index_hardwares_on_facility_id", using: :btree
  add_index "hardwares", ["hardware_status_id"], name: "index_hardwares_on_hardware_status_id", using: :btree
  add_index "hardwares", ["software_id"], name: "index_hardwares_on_software_id", using: :btree

  create_table "languages", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "softwares", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.date     "released_on"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "stakeholder_groups", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "stakeholders", force: true do |t|
    t.integer  "influence"
    t.integer  "interest"
    t.integer  "stakeholder_group_id"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "influence_target"
    t.integer  "interest_target"
  end

  add_index "stakeholders", ["stakeholder_group_id"], name: "index_stakeholders_on_stakeholder_group_id", using: :btree

  create_table "users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name"
    t.string   "role"
    t.datetime "locked_at"
    t.integer  "failed_attempts",        default: 0,  null: false
    t.boolean  "notifications"
    t.string   "phone"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end

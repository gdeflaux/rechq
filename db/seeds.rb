# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

def import_data(district_name, filename, lat, lng, has_hw)
  puts "     -> Data for #{district_name}"
  d = Facility.create(name: district_name, latitude: lat, longitude: lng)

  CSV.foreach("#{Rails.root}/db/fixtures/#{filename}") do |row|
    f = Facility.create(name: row[0], latitude: lat, longitude: lng, parent_id: d.id, population_total: row[1], population_under_five: row[2])
    Hardware.create(
      code: row[3],
      bought_on: row[4],
      inventory_on: row[4],
      software_id: 1,
      hardware_status_id: 1,
      brand: row[5],
      model: row[6],
      color: row[7],
      configuration: row[8],
      kind: row[9],
      facility_id: f.id
      ) if has_hw
  end 
end

def import_yako_hardware
  puts "     -> Hardware for Yako"
  
  d = Facility.find_by_name("Yako")
  
  CSV.foreach("#{Rails.root}/db/fixtures/yako_hardware.csv") do |row|
    f = Facility.find_by_name(row[0])
    h = Hardware.create(code: "TDH-BF-#{row[1]}", bought_on: "2013-10-31", inventory_on: "2014-05-12", hardware_status_id: 1,
                        brand: "Asus", model: "VivoBook X200CA" , color: "Noir", configuration: "Celeron 1007U 320Go 11.6",
                        kind: "Netbook", facility_id: f.id, software_id: 2)
    puts "        #{f.name} -> #{h.id.nil? ? "KO":"OK"}"
  end
end

puts "Seeding Database:"

puts "  -> Creating Languages"
if Language.count == 0
  Language.create([{name: "fr"}, {name: "en"}, {name: "es"}]) 
  puts "     OK"
else
  puts "     Already seeded"
end

puts "  -> Creating Hardware Status"
if HardwareStatus.count == 0
  HardwareStatus.create([{name: "production"}, {name: "development"}, {name: "stock"}, {name: "out_of_order"}])
  puts "     OK"
else
  puts "     Already seeded"
end

puts "  -> Creating Software"
if Software.count == 0
  Software.create(name: "REC v1.2", description: "Ajout de toutes les tranches d'âge PCIME", released_on: "2013-01-01")
  puts "     OK"
else
  puts "     Already seeded"
end

puts "  -> Creating Admin User"
if User.count == 0
  User.create(name: "Guillaume Deflaux", email: "gdf@tdh.ch", role: "admin", password: "qwertyuiop", password_confirmation: "qwertyuiop")
  puts "     OK"
else
  puts "     Already seeded"
end

puts "  -> Importing Facilities"
import_data("Tougan", "tougan.csv", 13.0697800237184, -3.06891918182373, true) if Facility.where(name: "Tougan").empty?
import_data("Séguénéga", "seguenega.csv", 13.4372920088813, -1.96285665035248, true) if Facility.where(name: "Séguénéga").empty?
import_data("Yako", "yako.csv", 12.956382587313202, -2.2623682022094727, false)  if Facility.where(name: "Yako").empty?
import_yako_hardware if Hardware.where(facility_id: Facility.find_by_name("Yako").id).empty?

puts "  -> Creating Stakeholder Groups"
if StakeholderGroup.count == 0
  StakeholderGroup.create(name: "UN Agencies")
  StakeholderGroup.create(name: "NGOs")
  puts "     OK"
else
  puts "     Already seeded"
end
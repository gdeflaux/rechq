class CreateFacilitiesUsersJoinTable < ActiveRecord::Migration
  def change
    create_join_table :facilities, :users do |t|
      t.index :facility_id
      t.index :user_id
    end
  end
end

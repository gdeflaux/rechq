class CreateStakeholderGroups < ActiveRecord::Migration
  def change
    create_table :stakeholder_groups do |t|
      t.string :name

      t.timestamps
    end
  end
end

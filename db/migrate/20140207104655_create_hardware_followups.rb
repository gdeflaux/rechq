class CreateHardwareFollowups < ActiveRecord::Migration
  def change
    create_table :hardware_followups do |t|
      t.belongs_to :hardware, index: true
      t.belongs_to :facility, index: true
      t.belongs_to :software, index: true
      t.text :description
      t.date :followup_on

      t.timestamps
    end
  end
end

class CreateFacilityFollowups < ActiveRecord::Migration
  def change
    create_table :facility_followups do |t|
      t.belongs_to :facility, index: true
      t.date :follow_up_on
      t.text :description

      t.timestamps
    end
  end
end

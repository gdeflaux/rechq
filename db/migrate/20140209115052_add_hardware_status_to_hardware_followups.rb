class AddHardwareStatusToHardwareFollowups < ActiveRecord::Migration
  def change
    add_reference :hardware_followups, :hardware_status, index: true
  end
end

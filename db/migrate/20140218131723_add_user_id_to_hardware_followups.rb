class AddUserIdToHardwareFollowups < ActiveRecord::Migration
  def change
    add_reference :hardware_followups, :user, index: true
  end
end

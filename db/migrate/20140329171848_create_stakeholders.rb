class CreateStakeholders < ActiveRecord::Migration
  def change
    create_table :stakeholders do |t|
      t.integer :influence
      t.integer :interest
      t.belongs_to :stakeholder_group, index: true
      t.string :name

      t.timestamps
    end
  end
end

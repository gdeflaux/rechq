class AddSoftwareToHardwares < ActiveRecord::Migration
  def change
    add_reference :hardwares, :software, index: true
  end
end

class AddFacilityToHardwares < ActiveRecord::Migration
  def change
    add_reference :hardwares, :facility, index: true
  end
end

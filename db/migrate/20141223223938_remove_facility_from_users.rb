class RemoveFacilityFromUsers < ActiveRecord::Migration
  def change
    remove_column :users, :facility_id
  end
end

class AddPopulationToFacilities < ActiveRecord::Migration
  def change
    add_column :facilities, :population_total, :integer
    add_column :facilities, :population_under_five, :integer
  end
end

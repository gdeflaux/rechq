class CreateHardwares < ActiveRecord::Migration
  def change
    create_table :hardwares do |t|
      t.string :code
      t.date :bought_on
      t.date :inventory_on
      t.belongs_to :hardware_status, index: true
      t.string :brand
      t.string :model
      t.string :color
      t.string :configuration
      t.string :kind

      t.timestamps
    end
  end
end

class AddInfluenceInterestTargetToStakeholders < ActiveRecord::Migration
  def change
    add_column :stakeholders, :influence_target, :integer
    add_column :stakeholders, :interest_target, :integer
  end
end

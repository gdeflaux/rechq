class AddUserIdToFacilityFollowups < ActiveRecord::Migration
  def change
    add_reference :facility_followups, :user, index: true
  end
end

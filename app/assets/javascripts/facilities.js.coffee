# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

jQuery ->
	$("#facilities_table").dataTable
		sPaginationType: "bootstrap"
		bProcessing: true
		bServerSide: true
		sAjaxSource: $("#facilities_table").data("source")
		oLanguage:
			sSearch: $("#sSearch").html()
			sLengthMenu: $("#sLengthMenu").html()
			sZeroRecords: $("#sZeroRecords").html()
			sInfo: $("#sInfo").html()
			sInfoEmpty: $("#sInfoEmpty").html()
			sInfoFiltered: $("#sInfoFiltered").html()
			oPaginate:
				sNext: $("#sNext").html()
				sPrevious: $("#sPrevious").html()
				
	$("#followups_table").dataTable
		sPaginationType: "bootstrap"
		bProcessing: true
		bServerSide: true
		bSort: false
		sAjaxSource: $("#followups_table").data("source")
		oLanguage:
			sSearch: $("#sSearch").html()
			sLengthMenu: $("#sLengthMenuFollowups").html()
			sZeroRecords: $("#sZeroRecordsFollowup").html()
			sInfo: $("#sInfo").html()
			sInfoEmpty: $("#sInfoEmpty").html()
			sInfoFiltered: $("#sInfoFilteredFollowups").html()
			oPaginate:
				sNext: $("#sNext").html()
				sPrevious: $("#sPrevious").html()
	
	# Hardware list toggle button
	$('#hw_list').on('hidden.bs.collapse', ->
		$('#hw_list_icon').prop('class', 'fa fa-arrow-circle-down')
		)
	$('#hw_list').on('shown.bs.collapse', ->
		$('#hw_list_icon').prop('class', 'fa fa-arrow-circle-up')
		)
		
	# Facility Follow Up list toggle button
	$('#ff_list').on('hidden.bs.collapse', ->
		$('#ff_list_icon').prop('class', 'fa fa-arrow-circle-down')
		)
	$('#ff_list').on('shown.bs.collapse', ->
		$('#ff_list_icon').prop('class', 'fa fa-arrow-circle-up')
		)

	# Map for the form
	marker = null
	
	updateFormCoordinates = (latLng) ->
	  $("#facility_latitude").val latLng.lat()
	  $("#facility_longitude").val latLng.lng()
	
	$('#map_wrapper').on('shown.bs.collapse', ->
		$('#facility_latitude').prop('readonly', true)
		$('#facility_longitude').prop('readonly', true)
		
		# Map center
		if ($('#facility_latitude').val() != "") && ($('#facility_longitude').val() != "")
			ll = new google.maps.LatLng($('#facility_latitude').val(), $('#facility_longitude').val());
		else
			ll = new google.maps.LatLng(12.361465967347373, -1.526477336883545); # Ouaga
		
		map = new google.maps.Map(document.getElementById("map_facility_form"), { center: ll, zoom: 5 })
		
		# Add marker if lat and long already set
		if ($('#facility_latitude').val() != "") && ($('#facility_longitude').val() != "")
			if marker != null
				marker.setMap(null)
			marker = new google.maps.Marker (
			  position: new google.maps.LatLng($('#facility_latitude').val(), $('#facility_longitude').val()),
			  title: ""
			)
			marker.setMap(map)
			
		# Replace marker on click	
		google.maps.event.addListener(map, 'click', (event) ->
			if marker != null
				marker.setMap(null)
			marker = new google.maps.Marker(
			  position: event.latLng
			  title: ""
			)
			marker.setMap(map);
			updateFormCoordinates(event.latLng)
		)
	)
	
	$('#map_wrapper').on('hidden.bs.collapse', ->
		$('#facility_latitude').prop('readonly', false)
		$('#facility_longitude').prop('readonly', false)
		)

	# Management of the population fields
	updatePopulationFields = ->
		if $('#facility_parent_id').val() == ""
			$('#facility_population_total').prop('readonly', true)
			$('#facility_population_total').val("")
			$('#facility_population_under_five').prop('readonly', true)
			$('#facility_population_under_five').val("")
		else
			$('#facility_population_total').prop('readonly', false)
			$('#facility_population_under_five').prop('readonly', false)
	
	updatePopulationFields()
	
	$('#facility_parent_id').change ->
		updatePopulationFields()
		
	
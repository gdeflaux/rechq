# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
#= require highcharts
#= require highcharts/highcharts-more
#= require jquery.ui.slider

jQuery ->
	
	$('#sh_graph-container').affix
		offset:
			top: 190 # pixel from top to graph title. Manually tested
	
	$.getJSON("/" + $("html").attr("lang") + "/stakeholders.json", (data) ->
		chart = new Highcharts.Chart
			chart:
				renderTo: 'sh_graph'
				type: 'bubble'
				zoomType: 'xy'
		
			title:
				text: data.translations.chart_title

			series: data.series

			plotOptions:					
				bubble:
					dataLabels:
						enabled: true
						formatter: ->
							@point.name
					maxSize: 60
					point:
						events:
							mouseOver: (e) ->
								ls = @series.chart.series.length
								target_serie = @series.chart.series[ls - 1]
								target_serie.addPoint([@x_t, @y_t, @z_t])
								
							mouseOut: (e) ->
								ls = @series.chart.series.length
								target_serie = @series.chart.series[ls - 1]
								l = target_serie.points.length
								target_serie.points[l - 1].remove(false) # false - otherwise the refresh messes with the bubble display
	
			xAxis:
				min: 0
				max: 7
				gridLineWidth: 1
				title:
					text: data.translations.xaxis

			yAxis:
				min: 0
				max: 7
				title:
					text: data.translations.yaxis

			tooltip:
				shared: true # To be able to display the tooltip from the hover event of the buttons outside the chart
				hideDelay: 0
				formatter: ->
					"<b>" + @point.name + "</b><br/>" +
					"<b>" + data.translations.xaxis + "</b>: " + @x + " => " + @point.x_t + 
					"<br/><b>" + data.translations.yaxis + "</b>: " + @y + " => " + @point.y_t
		
		$(".sh-link").mouseover( ->
			series_index = $(this).attr("data-group-index")
			point_index = $(this).attr("data-point-index")
			p = chart.series[series_index].data[point_index]
			ls = chart.series.length

			chart.series[ls - 1].addPoint([p.x_t, p.y_t, p.z_t])
			
			p.setState('hover')
			chart.tooltip.refresh(p)
		)
		
		$(".sh-link").mouseout( ->
			series_index = $(this).attr("data-group-index")
			point_index = $(this).attr("data-point-index")
			p = chart.series[series_index].data[point_index]
			p.setState('')
			
			series = chart.series[chart.series.length - 1]
			series.points[series.points.length - 1].remove(false)
			chart.tooltip.hide()
		)
		
		$(".sh-link-all").mouseover( ->
			n_series = chart.series.length
			series_index = 0
			while series_index < n_series
				n_points = chart.series[series_index].data.length
				point_index = 0
				while point_index < n_points
					p = chart.series[series_index].data[point_index]
					chart.series[n_series - 1].addPoint([p.x_t, p.y_t, p.z_t])
					point_index++
				series_index++
		)
		
		$(".sh-link-all").mouseout( ->
			n_series = chart.series.length
			n_points = chart.series[n_series - 1].data.length
			point_index = 0
			while point_index < n_points
				chart.series[n_series - 1].points[0].remove(false)
				point_index++
		)
	)
	
	$(".slider").slider(
		min: 1
		max: 6
		step: 1
		slide: (event, ui) ->
			$("#stakeholder_" + $(this).attr("data-field")).attr("value", ui.value)
			$(this).find("a").text(ui.value)
		create: (event, ui) ->
			val = $("#stakeholder_" + $(this).attr("data-field")).attr("value")
			$(this).slider("option", "value", val);
			$(this).find("a").text(val)
	)
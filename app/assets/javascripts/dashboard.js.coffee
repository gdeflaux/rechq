# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

jQuery ->
	map = new google.maps.Map(document.getElementById("map_dashboard"))
	
	$.getJSON("/" + $("html").attr("lang") + "/facilities/gmap_markers.json", (data) ->	
		bounds = new google.maps.LatLngBounds();
		markers = []
		for m in data
			marker = new google.maps.Marker({
				position: new google.maps.LatLng(m.lat, m.lng),
				map: map,
				title: m.title,
				icon: new google.maps.MarkerImage(m.icon, null, null, null, new google.maps.Size(25,37)),
				infowindow: new google.maps.InfoWindow({content: m.info}) # added property
			})
			bounds.extend(marker.position)
			markers.push(marker)
			google.maps.event.addListener(marker, 'click', ->
				this.infowindow.open(map, this)
			)
		map.fitBounds(bounds)
		new MarkerClusterer(map, markers, {gridSize: 30, maxZoom: 15})
	)
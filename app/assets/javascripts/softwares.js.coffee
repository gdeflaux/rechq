# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
jQuery ->
	$("#software_hardware_table").dataTable
	  sPaginationType: "bootstrap"
	  bSort: false
	  oLanguage:
	    sSearch: $("#sSearch").html()
	    sLengthMenu: $("#sLengthMenuH").html()
	    sZeroRecords: $("#sZeroRecordsH").html()
	    sInfo: $("#sInfo").html()
	    sInfoEmpty: $("#sInfoEmpty").html()
	    sInfoFiltered: $("#sInfoFilteredH").html()
	    oPaginate:
	      sNext: $("#sNext").html()
	      sPrevious: $("#sPrevious").html()
				
	$("#software_facilities_table").dataTable
	  sPaginationType: "bootstrap"
	  bSort: false
	  oLanguage:
	    sSearch: $("#sSearch").html()
	    sLengthMenu: $("#sLengthMenuF").html()
	    sZeroRecords: $("#sZeroRecordsF").html()
	    sInfo: $("#sInfo").html()
	    sInfoEmpty: $("#sInfoEmpty").html()
	    sInfoFiltered: $("#sInfoFilteredF").html()
	    oPaginate:
	      sNext: $("#sNext").html()
	      sPrevious: $("#sPrevious").html()
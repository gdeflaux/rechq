# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

jQuery ->
	# Facility Follow Up list toggle button
	$('#hwf_list').on('hidden.bs.collapse', ->
		$('#hwf_list_icon').prop('class', 'fa fa-arrow-circle-down')
		)
	$('#hwf_list').on('shown.bs.collapse', ->
		$('#hwf_list_icon').prop('class', 'fa fa-arrow-circle-up')
		)
				
	$("#hardware_table").dataTable
		sPaginationType: "bootstrap"
		bProcessing: true
		bServerSide: true
		sAjaxSource: $("#hardware_table").data("source")
		oLanguage:
			sSearch: $("#sSearch").html()
			sLengthMenu: $("#sLengthMenu").html()
			sZeroRecords: $("#sZeroRecords").html()
			sInfo: $("#sInfo").html()
			sInfoEmpty: $("#sInfoEmpty").html()
			sInfoFiltered: $("#sInfoFiltered").html()
			oPaginate:
				sNext: $("#sNext").html()
				sPrevious: $("#sPrevious").html()

	$("#followups_table").dataTable
		sPaginationType: "bootstrap"
		bSort: false
		bProcessing: true
		bServerSide: true
		sAjaxSource: $("#followups_table").data("source")
		oLanguage:
			sSearch: $("#sSearch").html()
			sLengthMenu: $("#sLengthMenuFollowups").html()
			sZeroRecords: $("#sZeroRecordsFollowup").html()
			sInfo: $("#sInfo").html()
			sInfoEmpty: $("#sInfoEmpty").html()
			sInfoFiltered: $("#sInfoFilteredFollowups").html()
			oPaginate:
				sNext: $("#sNext").html()
				sPrevious: $("#sPrevious").html()
class StakeholdersController < ApplicationController
  before_filter :authenticate_user!
  before_action :set_stakeholder, only: [:edit, :update, :destroy]
  load_and_authorize_resource
  
  # GET /stakeholders
  # GET /stakeholders.json
  def index
    respond_to do |format|
      format.html
      format.json do
        series = []
        StakeholderGroup.order(:id).each do |shg|
          h = {}
          h['name'] = shg.name
          h['data'] = []
          shg.stakeholders.order(:id).each do |sh|
            h['data'] << {x: sh.influence, y: sh.interest, z: 1, x_t: sh.influence_target, y_t: sh.interest_target, z_t: 1, name: sh.name}
          end
          series << h
        end
        series << {name: t('stakeholders.target'), data: []}
        json = {
                series: series,
                translations: {
                  chart_title: t('stakeholders.chart_title'), xaxis: t('stakeholders.xaxis'), yaxis: t('stakeholders.yaxis')
                }
              }
        render json: json # localized axis title, chart title
      end
    end
  end

  # GET /stakeholders/new
  def new
    @stakeholder = Stakeholder.new
  end

  # GET /stakeholders/1/edit
  def edit
  end

  # POST /stakeholders
  # POST /stakeholders.json
  def create
    @stakeholder = Stakeholder.new(stakeholder_params)

    respond_to do |format|
      if @stakeholder.save
        format.html { redirect_to stakeholders_url, notice: 'Stakeholder was successfully created.' }
      else
        format.html { render action: 'new' }
      end
    end
  end

  # PATCH/PUT /stakeholders/1
  # PATCH/PUT /stakeholders/1.json
  def update
    respond_to do |format|
      if @stakeholder.update(stakeholder_params)
        format.html { redirect_to stakeholders_url, notice: 'Stakeholder was successfully updated.' }
      else
        format.html { render action: 'edit' }
      end
    end
  end

  # DELETE /stakeholders/1
  # DELETE /stakeholders/1.json
  def destroy
    @stakeholder.destroy
    respond_to do |format|
      format.html { redirect_to stakeholders_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_stakeholder
      @stakeholder = Stakeholder.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def stakeholder_params
      params.require(:stakeholder).permit(:influence, :interest, :stakeholder_group_id, :name, :influence_target, :interest_target,)
    end
end

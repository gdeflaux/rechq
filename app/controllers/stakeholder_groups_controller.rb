class StakeholderGroupsController < ApplicationController
  before_filter :authenticate_user!
  before_action :set_stakeholder_group, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource
  
  # GET /stakeholder_groups
  # GET /stakeholder_groups.json
  def index
    @stakeholder_groups = StakeholderGroup.all
  end

  # GET /stakeholder_groups/new
  def new
    @stakeholder_group = StakeholderGroup.new
  end

  # GET /stakeholder_groups/1/edit
  def edit
  end

  # POST /stakeholder_groups
  # POST /stakeholder_groups.json
  def create
    @stakeholder_group = StakeholderGroup.new(stakeholder_group_params)

    respond_to do |format|
      if @stakeholder_group.save
        format.html { redirect_to stakeholder_groups_url, notice: 'Stakeholder group was successfully created.' }
      else
        format.html { render action: 'new' }
      end
    end
  end

  # PATCH/PUT /stakeholder_groups/1
  # PATCH/PUT /stakeholder_groups/1.json
  def update
    respond_to do |format|
      if @stakeholder_group.update(stakeholder_group_params)
        format.html { redirect_to stakeholder_groups_url, notice: 'Stakeholder group was successfully updated.' }
      else
        format.html { render action: 'edit' }
      end
    end
  end

  # DELETE /stakeholder_groups/1
  # DELETE /stakeholder_groups/1.json
  def destroy
    if @stakeholder_group.stakeholders.count > 0
      redirect_to stakeholder_groups_url, alert: "Delete associated stakeholders first"
    else
      @stakeholder_group.destroy
      redirect_to stakeholder_groups_url, notice: "Stakeholder group sucessfully deleted"
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_stakeholder_group
      @stakeholder_group = StakeholderGroup.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def stakeholder_group_params
      params.require(:stakeholder_group).permit(:name)
    end
end

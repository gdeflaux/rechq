class FacilitiesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_facility, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource

  # GET /facilities
  # GET /facilities.json
  def index
    @roots = current_user.facilities.order(name: :asc)
    @district = params['district'] && @roots.include?(Facility.find(params['district'])) ? Facility.find(params['district']) : @roots.first
    
    respond_to do |format|
      format.html
      format.json { render json: FacilitiesDatatable.new(view_context, @district) }
    end
  end

  # GET /facilities/1
  # GET /facilities/1.json
  def show
  end

  # GET /facilities/new
  def new
    @facility = Facility.new
  end

  # GET /facilities/1/edit
  def edit
  end

  # POST /facilities
  # POST /facilities.json
  def create
    @facility = Facility.new(facility_params)

    respond_to do |format|
      if @facility.save
        @facility.update_parent_populations
        format.html { redirect_to @facility, notice: 'Facility was successfully created.' }
        format.json { render action: 'show', status: :created, location: @facility }
      else
        format.html { render action: 'new' }
        format.json { render json: @facility.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /facilities/1
  # PATCH/PUT /facilities/1.json
  def update
    # Do not update the populations for a root as it is computed
    params[:facility].delete("population_total") if @facility.parent_id = ""
    params[:facility].delete("population_under_five") if @facility.parent_id = ""
    
    respond_to do |format|
      if @facility.update(facility_params)
        @facility.update_parent_populations
        format.html { redirect_to @facility, notice: 'Facility was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @facility.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /facilities/1
  # DELETE /facilities/1.json
  def destroy
    alert = ""
    alert += "Delete children first. " if @facility.has_children?
    alert += "Move hardwares to another facility first. " if @facility.hardwares.count > 0
    alert += "Delete hardware followup linked to facility first. " if @facility.hardware_followups.count > 0
    
    if alert != ""
      alert = "Cannot delete facility. " + alert
      redirect_to @facility, alert: alert
    else
      # Update parent population
      p = nil
      if !@facility.root?
        p = @facility.parent
        f_p_total = @facility.population_total
        f_p_u_five = @facility.population_under_five
      end
      
      @facility.destroy
      Facility.decrease_root_population(p, f_p_total, f_p_u_five) if !p.nil?
      
      redirect_to facilities_url, notice: "Facility deleted."
    end
  end

  def gmap_markers
    markers = []
    Facility.all.each do |f|
      icon = f.root? ? "district.png" : "health_center.png"
      markers << {
        title: f.name,
        lat: f.latitude,
        lng: f.longitude,
        icon: ActionController::Base.helpers.asset_url(icon),
        info: render_to_string(partial: "facilities/marker_info_window", :formats => [:html], locals: {facility: f})
      }
    end
    #marker.infowindow .gsub(/\n/, '').gsub(/"/, '\"')
    respond_to do |format|
      format.json { render json: markers }
    end
  end
  
  def update_gps
    @format_ok = true
    if params[:coordinates]
      coords = params[:coordinates].read
      CSV.parse(coords) do |row|
        if row.size != 2 and row[1].split(" ").size != 4
          @format_ok = false
          break
        end
      end
      if @format_ok
        @facilities_to_update = {}
        CSV.parse(coords) do |row|
          lat = row[1].split(" ")[0].to_f
          long = row[1].split(" ")[1].to_f
          @facilities_to_update[row[0].to_i] = {latitude: lat, longitude: long}
        end
        Facility.update(@facilities_to_update.keys, @facilities_to_update.values)
        @facilities_in_db = Facility.where(id: @facilities_to_update.keys).order(:name)
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_facility
      @facility = Facility.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def facility_params
      params.require(:facility).permit(:name, :ancestry, :parent_id, :latitude, :longitude, :population_total, :population_under_five)
    end
end

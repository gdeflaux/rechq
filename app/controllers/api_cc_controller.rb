class ApiCcController < ApplicationController
  require 'httpi'
  require 'httpclient'
  require 'json'
  
  def import_qi_reports
    url = "https://www.commcarehq.org/a/sara-csps/api/v0.5/case/?type=qi_report"
  end
  
  private
  
  def request(url)
    request = HTTPI::Request.new(url)
    request.auth.digest("gdf@tdh.ch", '31024"u56=#87+9Y')
    response = HTTPI.get(request, :httpclient)
  end
  
end
class FacilityFollowupsController < ApplicationController
  before_filter :authenticate_user!
  
  def index
    respond_to do |format|
      format.html
      format.json { render json: FacilityFollowupsDatatable.new(view_context, current_user) }
    end
  end
  
  def new
    @facility = Facility.find(params[:facility_id])
    @followup = @facility.facility_followups.build
    authorize! :create, @followup
  end
  
  def edit
    @facility = Facility.find(params[:facility_id])
    @followup = @facility.facility_followups.find(params[:id])
    authorize! :update, @followup
  end
  
  def create
    @facility = Facility.find(params[:facility_id])
    @followup = @facility.facility_followups.build(facility_followup_params)
    authorize! :create, @followup
    
    respond_to do |format|
      if @followup.save
        Notification.facility_followup_email(@followup).deliver
        format.html { redirect_to @facility, notice: 'Facility Follow Up was successfully created.' }
      else
        format.html { render action: 'new' }
      end
    end
  end
  
  def update
    @facility = Facility.find(params[:facility_id])
    @followup = @facility.facility_followups.find(params[:id])
    authorize! :update, @followup
    
    respond_to do |format|
      if @followup.update(facility_followup_params)
        format.html { redirect_to @facility, notice: 'Facility Follow Up was successfully updated.' }
      else
        format.html { render action: 'edit' }
      end
    end
  end
  
  def destroy
    @facility = Facility.find(params[:facility_id])
    @follow = FacilityFollowup.find(params[:id])
    authorize! :create, @follow
    @follow.destroy
    redirect_to @facility, notice: "Facility Follow Up successfully destroyed"
  end
  
  private
    # Never trust parameters from the scary internet, only allow the white list through.
    def facility_followup_params
      params.require(:facility_followup).permit(:description, :follow_up_on, :facility_id, :user_id)
    end
end

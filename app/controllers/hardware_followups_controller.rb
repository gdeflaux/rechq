class HardwareFollowupsController < ApplicationController
  before_filter :authenticate_user!
  
  def index
    respond_to do |format|
      format.html
      format.json { render json: HardwareFollowupsDatatable.new(view_context, current_user) }
    end
  end
  
  # GET /hardware_followups/new
  def new
    @hardware = Hardware.find(params[:hardware_id])
    @hardware_followup = HardwareFollowup.new
    @hardware_followup.facility_id = @hardware.facility_id
    authorize! :create, @hardware_followup
    
    @hardware_followup.hardware_status_id = @hardware.hardware_status_id
    @hardware_followup.software_id = @hardware.software_id
  end

  # GET /hardware_followups/1/edit
  def edit
    @hardware = Hardware.find(params[:hardware_id])
    @hardware_followup = @hardware.hardware_followups.find(params[:id])
    authorize! :update, @hardware_followup
  end

  # POST /hardware_followups
  # POST /hardware_followups.json
  def create
    @hardware = Hardware.find(params[:hardware_id])
    @hardware_followup = @hardware.hardware_followups.build(hardware_followup_params)
    authorize! :create, @hardware_followup
    
    if @hardware_followup.save
      Notification.hardware_followup_email(@hardware_followup).deliver
      redirect_to @hardware, notice: 'Hardware followup was successfully created.'
    else
      render action: 'new'
    end
  end

  # PATCH/PUT /hardware_followups/1
  # PATCH/PUT /hardware_followups/1.json
  def update
    @hardware = Hardware.find(params[:hardware_id])
    @hardware_followup = @hardware.hardware_followups.find(params[:id])
    authorize! :update, @hardware_followup
    
    respond_to do |format|
      if @hardware_followup.update(hardware_followup_params)
        format.html { redirect_to @hardware, notice: 'Hardware followup was successfully updated.' }
      else
        format.html { render action: 'edit' }
      end
    end
  end

  # DELETE /hardware_followups/1
  # DELETE /hardware_followups/1.json
  def destroy
    @hardware = Hardware.find(params[:hardware_id])
    @hardware_followup = HardwareFollowup.find(params[:id])
    authorize! :destroy, @hardware_followup
    
    @hardware_followup.destroy
    redirect_to @hardware, notice: "Hardware Follow Up successfully destroyed"
  end

  private
    # Never trust parameters from the scary internet, only allow the white list through.
    def hardware_followup_params
      params.require(:hardware_followup).permit(:hardware_id, :facility_id, :software_id, :hardware_status_id, :description, :followup_on, :user_id)
    end
end

class UsersController < ApplicationController
  before_filter :authenticate_user!
  before_action :set_user, only: [:show, :edit, :update, :destroy, :lock, :unlock]
  load_and_authorize_resource

  # GET /softwares
  # GET /softwares.json
  def index
    @users = User.all
  end

  # GET /softwares/1
  # GET /softwares/1.json
  def show
  end

  # GET /softwares/new
  def new
    @user = User.new
  end

  # GET /softwares/1/edit
  def edit
  end

  # POST /softwares
  # POST /softwares.json
  def create
    @user = User.new(user_params)

    respond_to do |format|
      if @user.save
        format.html { redirect_to @user, notice: 'User was successfully created.' }
        format.json { render action: 'show', status: :created, location: @user }
      else
        format.html { render action: 'new' }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /softwares/1
  # PATCH/PUT /softwares/1.json
  def update
    p = user_params
    if p[:password].blank?
      p.delete(:password)
      p.delete(:password_confirmation)
    end
    
    respond_to do |format|
      if @user.update(p)
        format.html { redirect_to @user, notice: 'User was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def lock
    if @user.is_lockable?(current_user)
      @user.lock_access!
      redirect_to @user, notice: "User locked"
    else
      redirect_to @user, alert: "Cannot lock user"
    end
  end
  
  def unlock
    if @user.is_lockable?(current_user)
      @user.unlock_access!
      redirect_to @user, notice: "User unlocked"
    else
      redirect_to @user, alert: "Cannot unlock user"
    end
  end

  # DELETE /softwares/1
  # DELETE /softwares/1.json
  def destroy
    alert = ""
    alert += "Delete follow ups first. " if @user.has_followups?
    alert += "User is the last Administrator." if @user.is_last_admin?
    
    if alert != ""
      alert = "Cannot delete user. " + alert
      redirect_to @user, alert: alert
    else
      @user.destroy
      redirect_to users_url, notice: "User deleted."
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:name, :email, :password, :password_confirmation,
                                    :role, :notifications, :phone,
                                    {facility_ids: []})
    end
end

class FacilityFollowup < ActiveRecord::Base
  belongs_to :facility
  belongs_to :user
  validates_presence_of :facility_id, :follow_up_on, :description, :user_id
  
  def self.date_last(facility)
    d = facility.facility_followups.order(follow_up_on: :desc).first
    d ? d.follow_up_on : "N/A"
  end
end

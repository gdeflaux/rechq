class Stakeholder < ActiveRecord::Base
  belongs_to :stakeholder_group
  
  validates_presence_of :name, :influence, :interest, :influence_target, :interest_target, :stakeholder_group_id
  validates_uniqueness_of :name
end

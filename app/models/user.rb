class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :recoverable, :rememberable, :trackable, :validatable, :lockable
  
  has_many :facility_followups
  has_many :hardware_followups
  has_and_belongs_to_many :facilities
  
  validates_presence_of :name, :phone

  validates_presence_of :email
  validates_uniqueness_of :email

  validates :password, presence: true, on: :create
  validates :password, confirmation: true

  validates_presence_of :role
  
  validate :has_facility_ids
  
  def has_facility_ids
    if facility_ids.length == 0
      errors.add(:facilitiy_ids, "must have access to at least one facility")
    end
  end
  
  def has_followups?
    (facility_followups.count + hardware_followups.count) > 0
  end
  
  def is_last_admin?
    (role == "admin") and (User.where(role: "admin").count == 1)
  end
  
  def is_destroyable?
    !(has_followups? or is_last_admin?)
  end
  
  def is_lockable?(current_user)
    !(is_last_admin? or id == current_user.id)
  end
  
  def authorized_facilities_ids
    af = []
    facilities.each do |f|
      af += f.children.map(&:id)
    end
    af += facilities.ids
    af
  end
  
  def authorized_facilities_grouped_select_options
    options = []
    facilities.order(:name).each do |r|
      options << [r.name, r.id]
      r.children.order(:name).each do |c|
        options << ["-- #{c.name}", c.id]
      end
    end
    options
  end
end

class Ability
  include CanCan::Ability

  def initialize(user)
    # Define abilities for the passed in user here. For example:
    #
    #   user ||= User.new # guest user (not logged in)
    #   if user.admin?
    #     can :manage, :all
    #   else
    #     can :read, :all
    #   end
    #
    # The first argument to `can` is the action you are giving the user 
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on. 
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/ryanb/cancan/wiki/Defining-Abilities
    
    user ||= User.new # guest user (not logged in)
    authorized_facilities_ids = user.authorized_facilities_ids
    
    if user.role == "viewer"
      can :read, :all
    end
    
    if user.role == "field_worker"
      can :read, Facility, id: authorized_facilities_ids
      can :read, Hardware, facility_id: authorized_facilities_ids
      
      can :read, FacilityFollowup
      can :create, FacilityFollowup, facility_id: authorized_facilities_ids
      can [:update, :destroy], FacilityFollowup, user_id: user.id, facility_id: authorized_facilities_ids
      
      can :read, HardwareFollowup
      can :create, HardwareFollowup, facility_id: authorized_facilities_ids
      can [:update, :destroy], HardwareFollowup, user_id: user.id, facility_id: authorized_facilities_ids
    end
    
    if user.role == "project_manager"
      can :read, Facility, id: authorized_facilities_ids
      can :create, Facility
      can [:update, :destroy], Facility, id: authorized_facilities_ids
      
      can :read, Hardware, facility_id: authorized_facilities_ids
      can :create, Hardware
      can [:update, :destroy], Hardware, facility_id: authorized_facilities_ids
      
      can :read, FacilityFollowup
      can :create, FacilityFollowup, facility_id: authorized_facilities_ids
      can [:update, :destroy], FacilityFollowup, facility_id: authorized_facilities_ids
      
      can :read, HardwareFollowup
      can :create, HardwareFollowup, facility_id: authorized_facilities_ids
      can [:update, :destroy], HardwareFollowup, facility_id: authorized_facilities_ids
    end
    
    if user.role == "admin"
      can :manage, :all
    end
    
  end
end

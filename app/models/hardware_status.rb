class HardwareStatus < ActiveRecord::Base
  def self.options_for_select
    options = []
    HardwareStatus.all.each do |hws|
      options << [hws.name, hws.id]
    end
    options
  end
end

class Facility < ActiveRecord::Base
  has_ancestry
  has_many :facility_followups, dependent: :destroy
  has_many :hardwares
  has_many :hardware_followups
  has_and_belongs_to_many :users
    
  validates_presence_of :name, :latitude, :longitude
  validates :name, uniqueness: { scope: :ancestry, message: "should be unique", case_sensitive: false }
  validate :not_my_own_parent
  validate :population_data
  
  after_validation :normalize, on: [ :create, :update ]
  
  def self.root?(node_id)
    Facility.roots.map{|r|r.id}.include?(node_id.to_i)
  end
  
  def not_my_own_parent
    if id && id == parent_id
      errors.add(:parent_id, "can't be its own parent")
    end
  end
  
  def population_data
    if parent_id && (population_total < population_under_five)
      errors.add(:population_under_five, "should be lower that total population")
    end
  end
  
  def update_parent_populations
    if !self.is_root?
      p = self.parent
      p.population_total = 0
      p.population_under_five = 0
      p.children.each do |c|
        p.population_total += c.population_total
        p.population_under_five += c.population_under_five
      end
      p.save!
    end
  end
  
  def self.decrease_root_population(p, f_p_total, f_p_u_five)
    if p.root?
      p.population_total -= f_p_total
      p.population_under_five -= f_p_u_five
      p.save!
    end
  end
    
  protected
  
  def normalize
    self.name = self.name.downcase.titleize
  end
  
end

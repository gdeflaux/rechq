class Software < ActiveRecord::Base
  has_many :hardwares
  has_many :hardware_followups
  has_many :facilities, through: :hardwares
  has_many :hardware_statuses, through: :hardwares
  
  validates_presence_of :name, :description, :released_on
end

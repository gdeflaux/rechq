class Language < ActiveRecord::Base
  def self.current
    Language.where(name: I18n.locale.to_s).first.name
  end
end

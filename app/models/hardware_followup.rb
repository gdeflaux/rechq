class HardwareFollowup < ActiveRecord::Base
  belongs_to :hardware
  belongs_to :facility
  belongs_to :software
  belongs_to :user
  belongs_to :hardware_status
  
  validates_presence_of :hardware_id, :facility_id, :software_id, :hardware_status_id, :description, :followup_on, :user_id
  
  def self.date_last(hw)
    d = hw.hardware_followups.order(followup_on: :desc).first
    d ? d.followup_on : "N/A"
  end
end

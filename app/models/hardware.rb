class Hardware < ActiveRecord::Base
  belongs_to :hardware_status
  belongs_to :facility
  belongs_to :software
  has_many :hardware_followups, dependent: :destroy

  validates_presence_of :code, :bought_on, :inventory_on, :hardware_status_id,
                        :brand, :model, :color, :configuration, :kind, :facility_id, :software_id
  
  validates_uniqueness_of :code
end

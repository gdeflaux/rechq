class StakeholderGroup < ActiveRecord::Base
  has_many :stakeholders
  validates_presence_of :name
  validates_uniqueness_of :name
  
  def self.options_for_select
    options = []
    StakeholderGroup.all.each do |shg|
      options << [shg.name, shg.id]
    end
    options
  end
end

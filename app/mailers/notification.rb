class Notification < ActionMailer::Base
  default from: "no-reply@hq.ieda-project.com"
  
  def hardware_followup_email(followup)
    to = User.where(notifications: true).map(&:email).join(',')
    @followup = followup
    mail to: to, subject: t('notifications.hardware.subject')
  end
  
  def facility_followup_email(followup)
    to = User.where(notifications: true).map(&:email).join(',')
    @followup = followup
    mail to: to, subject: t('notifications.facility.subject')
  end
end

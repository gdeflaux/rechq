class ApiNotification < ActionMailer::Base
  default from: "no-reply@hq.ieda-project.com"
    
  def incoming(sender, data)
    to = "gdf@tdh.ch"
    @sender = sender
    @data = data
    
    mail to: to, subject: "API - Incoming - #{sender}"
  end
  
end

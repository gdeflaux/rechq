module UsersHelper
  def role_options
    [[t('roles.viewer'), "viewer"], [t('roles.field_worker'), "field_worker"], [t('roles.project_manager'), "project_manager"],[t('roles.admin'), "admin"]]
  end
end

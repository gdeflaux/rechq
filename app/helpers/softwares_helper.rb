module SoftwaresHelper
  def gmap_facility_markers(software)
    markers = []
    software.facilities.distinct.each do |f|
      markers << "#{f.latitude},#{f.longitude}"
    end
    markers.join('|')
  end
end

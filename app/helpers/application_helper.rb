module ApplicationHelper
  def link_to_icon(text, url, icon, options = {})
    link_to "<i class=\"#{icon}\"></i> #{text}".html_safe, url, options
  end  
end

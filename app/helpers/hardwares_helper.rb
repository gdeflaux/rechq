module HardwaresHelper
  def translate_hws_options_for_select(options)
    options.each do |o| 
      o[0] = t("hardware_status.#{o[0]}")
    end
  end
end

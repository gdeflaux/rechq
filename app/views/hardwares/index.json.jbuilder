json.array!(@hardwares) do |hardware|
  json.extract! hardware, :id, :code, :bought_on, :inventory_on, :hardware_status_id, :brand, :model, :color, :configuration, :kind
  json.url hardware_url(hardware, format: :json)
end

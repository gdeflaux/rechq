json.extract! @hardware, :id, :code, :bought_on, :inventory_on, :hardware_status_id, :brand, :model, :color, :configuration, :kind, :created_at, :updated_at

json.array!(@facilities) do |facility|
  json.extract! facility, :id, :name, :ancestry, :latitude, :longitude
  json.url facility_url(facility, format: :json)
end

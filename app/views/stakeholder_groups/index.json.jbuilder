json.array!(@stakeholder_groups) do |stakeholder_group|
  json.extract! stakeholder_group, :id, :name
  json.url stakeholder_group_url(stakeholder_group, format: :json)
end

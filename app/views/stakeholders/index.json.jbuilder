json.array!(@stakeholders) do |stakeholder|
  json.extract! stakeholder, :id, :influence, :interest, :stakeholder_group_id, :name
  json.url stakeholder_url(stakeholder, format: :json)
end

class HardwareFollowupsDatatable
  delegate :params, :link_to, to: :@view

  def initialize(view, current_user)
    @view = view
    @current_user = current_user
  end

  def as_json(options = {})
    {
      sEcho: params[:sEcho].to_i,
      iTotalRecords: followups.total_entries,
      iTotalDisplayRecords: followups.total_entries,
      aaData: data
    }
  end

private

  def data
    followups.map do |followup|
      [
        @view.render(partial: "hardwares/latest_followups_item", formats: "html", locale: I18n.locale, locals: {ff: followup}).html_safe
      ]
    end
  end

  def followups
    @followups ||= fetch_followups
  end

  def fetch_followups
    followups = HardwareFollowup.joins(:hardware, :user, :facility, :hardware_status).select("*, users.name as user_name, facilities.name as facility_name, hardwares.code as hardware_code, hardware_statuses.name as status_name").where("hardware_followups.facility_id IN (#{@current_user.authorized_facilities_ids.join(",")})")
    followups = followups.order(followup_on: :desc, id: :desc)
    followups = followups.page(page).per_page(per_page)
    if params[:sSearch].present?
      followups = followups.where("description ILIKE :search OR users.name ILIKE :search OR facilities.name ILIKE :search OR hardwares.code ILIKE :search", search: "%#{params[:sSearch]}%")
    end
    followups
  end

  def page
    params[:iDisplayStart].to_i/per_page + 1
  end

  def per_page
    params[:iDisplayLength].to_i > 0 ? params[:iDisplayLength].to_i : 10
  end
end
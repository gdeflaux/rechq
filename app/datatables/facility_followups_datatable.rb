class FacilityFollowupsDatatable
  delegate :params, :link_to, to: :@view

  def initialize(view, current_user)
    @view = view
    @current_user = current_user
  end

  def as_json(options = {})
    {
      sEcho: params[:sEcho].to_i,
      iTotalRecords: followups.total_entries,
      iTotalDisplayRecords: followups.total_entries,
      aaData: data
    }
  end

private

  def data
    followups.map do |followup|
      [
        @view.render(partial: "facilities/latest_followups_item", formats: "html", locale: I18n.locale, locals: {ff: followup}).html_safe
      ]
    end
  end

  def followups
    @followups ||= fetch_followups
  end

  def fetch_followups
    followups = FacilityFollowup.joins(:user, :facility).select("*, users.name AS user_name, facilities.name AS facility_name").where("facility_followups.facility_id IN (#{@current_user.authorized_facilities_ids.join(",")})")
    followups = followups.order(follow_up_on: :desc, id: :desc)
    followups = followups.page(page).per_page(per_page)
    if params[:sSearch].present?
      followups = followups.where("description ILIKE :search OR users.name ILIKE :search OR facilities.name ILIKE :search", search: "%#{params[:sSearch]}%")
    end
    followups
  end

  def page
    params[:iDisplayStart].to_i/per_page + 1
  end

  def per_page
    params[:iDisplayLength].to_i > 0 ? params[:iDisplayLength].to_i : 10
  end
end
class FacilitiesDatatable
  delegate :params, :link_to, to: :@view

  def initialize(view, district)
    @view = view
    @district = district
  end

  def as_json(options = {})
    {
      sEcho: params[:sEcho].to_i,
      iTotalRecords: facilities.total_entries,
      iTotalDisplayRecords: facilities.total_entries,
      aaData: data
    }
  end

private

  def data
    facilities.map do |facility|
      district_marker = facility.id == @district.id ? ' <i class="fa fa-home"></i>'.html_safe : ""
      [
        link_to(facility.name, facility) + district_marker,
        facility.population_total || 0,
        facility.population_under_five || 0,
        facility.last_followup ? facility.last_followup : "N/A",
        facility.n_hardware
      ]
    end
  end

  def facilities
    @facilities ||= fetch_facilities
  end

  def fetch_facilities
    facilities = Facility.select("*, (SELECT COUNT(*) FROM hardwares WHERE hardwares.facility_id = facilities.id) AS n_hardware, (SELECT follow_up_on FROM facility_followups WHERE facility_followups.facility_id = facilities.id ORDER BY follow_up_on DESC LIMIT 1) AS last_followup").where(id: @district.children.map(&:id) << @district.id)
    facilities = facilities.order("#{sort_column} #{sort_direction}")
    facilities = facilities.page(page).per_page(per_page)
    if params[:sSearch].present?
      facilities = facilities.where("name ILIKE :search", search: "%#{params[:sSearch]}%")
    end
    facilities
  end

  def page
    params[:iDisplayStart].to_i/per_page + 1
  end

  def per_page
    params[:iDisplayLength].to_i > 0 ? params[:iDisplayLength].to_i : 10
  end

  def sort_column
    columns = %w[name population_total population_under_five last_followup n_hardware]
    columns[params[:iSortCol_0].to_i]
  end

  def sort_direction
    params[:sSortDir_0] == "desc" ? "desc" : "asc"
  end
end
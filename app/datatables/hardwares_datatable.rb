class HardwaresDatatable
  delegate :params, :link_to, to: :@view

  def initialize(view, current_user)
    @view = view
    @current_user = current_user
  end

  def as_json(options = {})
    {
      sEcho: params[:sEcho].to_i,
      iTotalRecords: hardwares.total_entries,
      iTotalDisplayRecords: hardwares.total_entries,
      aaData: data
    }
  end

private

  def data
    hardwares.map do |hardware|
      [
        link_to(hardware.code, hardware),
        hardware.facility_name,
        @view.t("hardware_status.#{hardware.status_name}"),
        hardware.software_name,
        hardware.last_followup || "N/A"
      ]
    end
  end

  def hardwares
    @hardwares ||= fetch_hardwares
  end
  
  def fetch_hardwares
    hardwares = Hardware.joins(:software, :facility, :hardware_status).select("hardwares.*, softwares.name as software_name, facilities.name as facility_name, hardware_statuses.name as status_name, (SELECT followup_on FROM hardware_followups WHERE hardware_followups.hardware_id = hardwares.id ORDER BY followup_on DESC LIMIT 1) AS last_followup").where("hardwares.facility_id IN (#{@current_user.authorized_facilities_ids.join(",")})")
    hardwares = hardwares.order("#{sort_column} #{sort_direction}")
    hardwares = hardwares.page(page).per_page(per_page)
    if params[:sSearch].present?
      hardwares = hardwares.where("code ILIKE :search", search: "%#{params[:sSearch]}%")
    end
    hardwares
  end

  def page
    params[:iDisplayStart].to_i/per_page + 1
  end

  def per_page
    params[:iDisplayLength].to_i > 0 ? params[:iDisplayLength].to_i : 10
  end

  def sort_column
    columns = %w[code facility_name status_name software_name last_followup]
    columns[params[:iSortCol_0].to_i]
  end

  def sort_direction
    params[:sSortDir_0] == "desc" ? "desc" : "asc"
  end
end
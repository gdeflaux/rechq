require 'test_helper'

class HardwareFollowupsControllerTest < ActionController::TestCase
  setup do
    @hardware_followup = hardware_followups(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:hardware_followups)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create hardware_followup" do
    assert_difference('HardwareFollowup.count') do
      post :create, hardware_followup: { description: @hardware_followup.description, facility_id: @hardware_followup.facility_id, followup_on: @hardware_followup.followup_on, hardware_id: @hardware_followup.hardware_id }
    end

    assert_redirected_to hardware_followup_path(assigns(:hardware_followup))
  end

  test "should show hardware_followup" do
    get :show, id: @hardware_followup
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @hardware_followup
    assert_response :success
  end

  test "should update hardware_followup" do
    patch :update, id: @hardware_followup, hardware_followup: { description: @hardware_followup.description, facility_id: @hardware_followup.facility_id, followup_on: @hardware_followup.followup_on, hardware_id: @hardware_followup.hardware_id }
    assert_redirected_to hardware_followup_path(assigns(:hardware_followup))
  end

  test "should destroy hardware_followup" do
    assert_difference('HardwareFollowup.count', -1) do
      delete :destroy, id: @hardware_followup
    end

    assert_redirected_to hardware_followups_path
  end
end

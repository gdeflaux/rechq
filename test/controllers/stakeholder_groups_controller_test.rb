require 'test_helper'

class StakeholderGroupsControllerTest < ActionController::TestCase
  setup do
    @stakeholder_group = stakeholder_groups(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:stakeholder_groups)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create stakeholder_group" do
    assert_difference('StakeholderGroup.count') do
      post :create, stakeholder_group: { name: @stakeholder_group.name }
    end

    assert_redirected_to stakeholder_group_path(assigns(:stakeholder_group))
  end

  test "should show stakeholder_group" do
    get :show, id: @stakeholder_group
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @stakeholder_group
    assert_response :success
  end

  test "should update stakeholder_group" do
    patch :update, id: @stakeholder_group, stakeholder_group: { name: @stakeholder_group.name }
    assert_redirected_to stakeholder_group_path(assigns(:stakeholder_group))
  end

  test "should destroy stakeholder_group" do
    assert_difference('StakeholderGroup.count', -1) do
      delete :destroy, id: @stakeholder_group
    end

    assert_redirected_to stakeholder_groups_path
  end
end

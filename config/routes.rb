Rechq::Application.routes.draw do
  scope "/:locale" do
    
    devise_for :users

    resources :stakeholder_groups, except: [:show]
    resources :stakeholders, except: [:show]
    
    resources :users do
      member do
        get 'lock'
        get 'unlock'
      end
    end
    
    get 'dashboard' => 'dashboard#index'
    
    get "/facilities/gmap_markers" => 'facilities#gmap_markers'
    get "/facilities/update_gps" => 'facilities#update_gps'
    post "/facilities/update_gps" => 'facilities#update_gps'
        
    resources :softwares

    get "/hardware_followups" => 'hardware_followups#index'

    resources :hardwares do
      resources :hardware_followups, except: [:index, :show]
    end
  
    get "/facility_followups" => 'facility_followups#index'
    
    resources :facilities do
      resources :facility_followups, except: [:index, :show]
    end
    
  end
  
  # CommCare API integration
  post "/api_cc/incoming" => 'api_cc#incoming'
  get "/api_cc/import" => 'api_cc#import'
  
  get "/:locale" => 'dashboard#index'
  root :to => 'dashboard#index'
  
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
